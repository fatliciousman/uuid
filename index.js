const path = require('path')
const { v4: uuidv4} = require ('uuid');
const express = require ('express')
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true}))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

const comments = [ 
    {
        id: uuidv4(),
    username:'Brian',
    text: `Freelancer`
    },
    {
        id: uuidv4(),
     username:'Naca',
        text: `Seorang pengusaha`
    },
    {
        id: uuidv4(),
        username:'Elizabeth',
           text: `Suka sekali bermain masak - masakan`
       },
       {
        id: uuidv4(),
        username:'James',
           text: `Sangat menyukai robot`
       },
    ];

    app.get('/comments', (req, res) => {
        res.render('comments/index', {comments})
    })
    
app.get('/comments/create', (req, res) => {
    res.render('comments/create')
})

app.post('/comments', (req, res) => {
    const {username, text} = req.body
    comments.push ({username, text, id: uuidv4()})
    res.redirect('/comments')
})

app.get('/comments/:id', (req, res)=>{
    const {id} = req.params
    const comment = comments.find(c => c.id === id)
    res.render('comments/show', {comment})
})

app.get('/order', (req, res) => {
    res.send('GET Order Response')
});

app.post('/order', (req, res) => {
    const {item, qty} = req.body
    res.send(`Item: ${item} - Qty: ${qty}` )
});


app.listen(8080, () => {
    console.log(`Server is running on: http://localhost:8080`)
});